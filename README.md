# snake-snake
Construct 2 project source for Snake? Snake!

### Game description ###

Do you think (brotherly) love can bloom on the battlefield? Find out in Snake Snake! Choose your favorite collection of muscle mass and try to recruit more soldiers than your twin brother before time runs out!

### What is this? ###

[Snake? Snake!](http://virtually-competent.itch.io/snake-snake) was created in about two weeks for the [Duplicade](http://itch.io/jam/duplicade) jam and parodies the Metal Gear Solid series as well as the classic "Snake" games. The game was well received, taking 2nd place overall out of 64 entries, but we don't have any plans to update or sell the game, so it seemed appropriate to make the source available to the public.

### How do I get set up? ###

The source for Snake Snake was created in [Scirra Construct 2](https://www.scirra.com/construct2), so you'll need that program to edit/run the project. You should be able to open it with the free version, but you'll have limited editing abilities and won't be able to export the game as the event count is just slightly over the limit of 100.

### Who do I talk to? ###

As I already mentioned, Snake Snake has been abandoned and is no longer in development. However, if you need to hold someone accountable for this mess, you can contact [@derekandes on Twitter](https://twitter.com/derekandes).
